import random , math , copy
import numpy as np
import matplotlib.pyplot as plt

dimension = 22
n_clusters = 2

default_cluster = -1
default_distance = 1
index_cluster = dimension
index_min_dist = index_cluster + 1

def generate_point():
	point = []
	for i in range(dimension):
		point.append(random.random())
	point.append(default_cluster)
	return point


def initial_clusters():
	centroids = []
	for i in range(n_clusters):
		point = generate_point()
		centroids.append(point)
	return centroids


def distance(p1 , p2):
	'''euclidean distance'''
	d = 0
	for i in range(dimension):
		d += (p1[i] - p2[i])**2
	return math.sqrt(d)

def sum_points(p1 , p2):
	new_p = generate_point()
	for i in range(dimension):
		new_p[i] = p1[i] + p2[i]
	return new_p


def ppoints(points):
	for i in points:
		for j in i:
			print format(j , '.4f') , 
		print

def div_scalar(p , x):
	return list(map((lambda s : s/x) , p))

def calculate_groups(points , centroids , n_elem_centroids ):
	'''calculate groups'''
	for i in range(len(n_elem_centroids)):
		n_elem_centroids[i] = 0
	for p in points:
		distances = []
		for c in centroids:
			distances.append(distance(p , c))

		p[index_cluster] = distances.index(min(distances))
		n_elem_centroids[int(p[index_cluster])] += 1

def update_centroids(points, new_centroids , n_elem_centroids ):
	'''update centroids'''
	for p in points:
		new_centroids[ int(p[index_cluster]) ] = sum_points (new_centroids [int(p[index_cluster])], p)

	for i in range(n_clusters):
		if n_elem_centroids[i] > 0:
			new_centroids[i] = div_scalar(new_centroids[i] , n_elem_centroids[i])

def calculate_shift(centroids , new_centroids):
	'''calculate shift of centroids'''
	diferences = [0]*n_clusters
	for i in range(n_clusters):
		diferences[i] = distance(centroids[i] , new_centroids[i])

	shift = max(diferences)
	return shift

def variance(points , centroids):

	groups = []
	for n in range(n_clusters):
		clust = []	
		for p in points:
			if p[index_cluster] == n: clust.append(p)
		groups.append(clust)

	for i in range(n_clusters):
		print 'cluster' , i
		dist = 0
		smtory = 0
		for p in groups[i]:
			dis = distance(p , centroids[i])
			if dis > dist : dist = dis 
			smtory += dis
		print 'intra-cluster' , dist
		variance = smtory / len(groups[i])
		# print 'variance\t' , variance
		standard_deviation = math.sqrt(variance)
		print 'standard deviation' , standard_deviation

def displ_color(points, color):
	for p in points:
		x = []
		y = []
		x.append(p[0])
		y.append(p[1])
		plt.plot(x,y,color)


def test():
	# random.seed(100)
	# data = open('../data_sets/wine.txt' , 'r').read().split('\n')
	data = open('../data_sets/spect.txt' , 'r').read().split('\n')

	data_ = []
	for i in data:
		s = i.split(',')[1:dimension+1]
		# s = i.split(',')[:-1]
		s = [i for i in s if i!='']
		print s
		d = [float(j) for j in s]
		d.append(default_cluster)
		data_.append(d)

	print data_

	'''normalize'''
	data_ = np.array(data_)
	data__=np.transpose(data_)

	for i in range(len(data__) -1):
		data__[i] -= min(data__[i])
		data__[i] /= max(data__[i])
	points = np.transpose(data__)

	'''centroids'''
	centroids = []
	for i in range(n_clusters):
		centroids.append(points[random.randrange(len(points))])
	
	new_centroids = [[0.0]*dimension]*n_clusters
	n_elem_centroids = [0]*n_clusters
	max_shift = 0.00005

	shift = 1
	iteracion = 1

	print 'iteracion'
	while(shift > max_shift):
		print  iteracion ,
		iteracion+=1

		new_centroids = [[0.0]*dimension]*n_clusters
		calculate_groups(points , centroids , n_elem_centroids)
		update_centroids(points , new_centroids , n_elem_centroids)
		shift = calculate_shift(centroids , new_centroids)
		centroids = copy.deepcopy(new_centroids)

	print '\ninter-cluster distances'
	for i in centroids:
		for j in centroids:
			print distance(i,j) ,"\t\t\t" , 
		print

	variance(points , centroids)

	ppoints(points)
	# ppoints(centroids)
	# displ_color(points , 'ro')
	# displ_color(centroids, 'b^')

	# plt.show()

if __name__ == "__main__":
	test()















