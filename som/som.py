
import numpy as np
import random , copy
from operator import itemgetter
import itertools

import matplotlib.pyplot as plt


DIM = 2
POINTS = 50
NEURONS = 30
ALFA = 0.3
MAX_SHIFT = 0.05

DEFAULT_GROUP = -1

class Point:

	def __init__(self , random=False):
		self.data = np.array([0.0]*DIM)
		self.group = DEFAULT_GROUP
		if random == True :
			self.randomize()

	def randomize(self):
		self.data = np.array([random.random() for i in range(DIM)])

	def set_position(self , pos):
		self.data = np.array(pos)

	def distance(self , point):
		return np.linalg.norm(point.data - self.data)		

def generate_random(n):
	return [ Point(True) for i in range(n)]

class Som:

	def __init__(self , points , neurons):
		print 'creating'

		self.iteration = 1
		self.shift = 1
		self.points = points
		self.neurons = neurons
		self.new_neurons = copy.deepcopy(self.neurons)

		while self.shift > MAX_SHIFT :
			self.fit_neurons()
			self.shift = self.calculate_shift()
			self.neurons = copy.deepcopy(self.new_neurons)
			print self.shift

		# self.update_neurons()

	def fit_neurons(self):

		print 'calculating' , self.iteration
		for p in self.points:
			neuron_i =  min(enumerate([n.distance(p) for n in self.neurons]) , key=itemgetter(1))[0]
			self.new_neurons[neuron_i].data += (p.data - self.new_neurons[neuron_i].data)*self.alfa(self.iteration) 
			# print neuron_i , self.new_neurons[neuron_i].data , p.data


		self.iteration += 1

	def calculate_shift(self):

		return max([self.neurons[i].distance(self.new_neurons[i]) for i in range(len(neurons))])

	def alfa(self , n):
		return 1./n;

class SomKD:

	def __init__(self , points , division):

		self.iteration = 1
		self.shift = 1
		self.points = points
		self.dimension = 2
		self.division = division

		self.neurons = self.get_neurons_position()
		self.new_neurons = copy.deepcopy(self.neurons)

		self.show_points(self.neurons , 'bo')

		while self.shift > MAX_SHIFT:
			self.fit_neurons()
			self.shift = self.calculate_shift()
			self.neurons = copy.deepcopy(self.new_neurons)
			print self.shift

		self.show_points(self.points , 'ro')
		self.show_points(self.neurons, 'g^')

		plt.show()
		


	def fit_neurons(self):

		print 'calculating' , self.iteration
		for p in self.points:
			neuron_i = min(enumerate([n.distance(p) for n in self.neurons]) , key=itemgetter(1))[0]
			self.new_neurons[neuron_i].data += (p.data - self.new_neurons[neuron_i].data)*self.alfa(self.iteration)

		self.iteration += 1

	def calculate_shift(self):
		return max([self.neurons[i].distance(self.new_neurons[i]) for i in range(len(self.neurons))])
		print 'calculating shift'

	def alfa(self , n):
		return 1./n

	def get_neurons_position(self):

		bounds = []
		data = [p.data for p in self.points]

		for i in data: print i
		matrix = np.transpose(np.array(data))

		for i in matrix:
			bounds.append([max(i) , min(i)])

		print bounds
		sections = []

		for i in range(self.dimension):

			s = (bounds[i][0] - bounds[i][1])/self.division[i]
			f = bounds[i][1]
			section = []
			for j in range(self.division[i]+1):
				section.append(f)
				f += s
			sections.append(section)

		# print sections

		neuron_points = np.array(list(itertools.product(*sections)))

		neurons = []
		for i in neuron_points:
			n = Point()
			n.set_position(i)
			neurons.append(n)

		return neurons

	def show_points(self , points , color):
		for p in points:
			x = []
			y = []
			x.append(p.data[0])
			y.append(p.data[1])
			plt.plot(x , y , color)




			




random.seed(0)
points = generate_random(POINTS)
neurons = generate_random(NEURONS)


SomKD(points ,  [3 , 2])


	# i.show()
